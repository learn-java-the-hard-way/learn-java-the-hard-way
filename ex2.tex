\chapter{Exercise 2: Primitive Types and Strings}

Now that you have seen a very basic Java program, we're going to dig into 
the language a little deeper.  The first place we are going to touch on
are the primitive in data types of the language and one of the most common
(in my humble opinion) of the \textbf{Object} based data types, \textbf{String}.

Java is primarily a statically typed language, which is to say the data types
are defined when a variable is created, this is similar to C/C++.  Java also has
the ability to dynamically cast a variable from one type to another, so long as
the casing being done is downcasting. 

\begin{aside}{Downcasting}
Downcasting refers to the act of casting a reference of a base class to one of its
derived classes.  For instance, you can cast a \textbf{byte} value into an \textbf{int}
variable.
\end{aside}

There are eight primitive data types in Java:

\begin{itemize}
\item byte:  This type is an 8-bit signed two's complement integer. It has a range
			 of -128 to 127.
\item short: This type is a 16-bit signed two's complement integer.  It has a range
			 of -32,768 to 32,767.
\item int: This type is a 32-bit signed two's complement integer.  It has a range
		   of -2,147,483,648 to 2,147,483,647.
\item long: This type is the largest of the integer types.  It is a 64-bit signed
		    two's complement integer with a range of -9,223,372,036,854,775,808 to
		    9,223,372,036,854,775,807
\item float: This data type is a single-precision 32-bit floating point.  Due to the 
			 make-up of the type it is difficult to give the full range.
\item double: This data type is a double-precision 64-bit floating point.  Due to the 
			  make-up of the type it is difficult to give the full range.
\item boolean: This data type has only two values, \textbf{true} and \textbf{false}.
\item char: This data type is used to represent a single 16-bit Unicode character.
\end{itemize}

These primitive data types, along with the \textbf{String} become very useful as a 
starting foundation for building your own classes (something that we will be covering
later).

\begin{aside}{Initializing Variables}
It is always a good practice to initialize variables when you create them.  This can
help alleviate errors later in your program.  \textbf{long}s, \textbf{float}s, and
\textbf{double}s require a special character at the end of the number when performing
initialization.  The table below shows the default initial values for data types, however,
as I said before, it is always good practice to initialize yourself, even if it is 
just to the default value.


\begin{tabular} { | l | c | }
	\hline	
		\textbf{Data Type} & \textbf{Default Value} \\ \hline
		byte & 0 \\ \hline
		short & 0 \\ \hline
		long & 0L \\ \hline
		float & 0.0f \\ \hline
		double & 0.0d \\ \hline
		char & '\u0000' \\ \hline
		String (or any Object) & null \\ \hline
		boolean & false \\ \hline
	\hline
\end{tabular}


Note the differences with the default values for \textbf{long}, \textbf{float} and 
\textbf{double}.  Any time you are going to hard code a value for a variable of
one of these types you should include the trailing character.  It is also good to
know that the default value for the \textbf{char} is Unicode representation of the
character.

\end{aside}

Here is an example program showcasing the data types:

\begin{code}{ex2a.java}
<< d['code/ex2a.java|pyg|l'] >>
\end{code}

This is a pretty simple example that simply shows how you would declare a variable
of each type and assign a value.  

\begin{aside}{Variable Naming}
I would like to point out here that the variable names used in this example are 
horrible names.  In a read world program variable names should be defined in order 
to be descriptive about the purpose of the data.  For instance: a variable used to
hold a count would probably be named count or cnt.
\end{aside}

To run the program run the following from a terminal:

\begin{code}{Building ex2a}
\begin{lstlisting}

user@localhost ~/ $ javac LearnJavaTheHardWay/ex2a.java
user@localhost ~/ $ java LearnJavaTheHardWay/ex2a
100
28234
4323454
2345654321123
1.45
3.324212345656443
d
true
This is a String!
\end{lstlisting}
\end{code}

\section{Downcasting}

As mentioned previously, Java does support downcasting in certain instances.  
Downcasting can be performed based on the following conversions:

\begin{itemize}
\item \textbf{byte} to \textbf{short}, \textbf{int}, \textbf{long}, \textbf{float}, or
	  \textbf{double}
\item \textbf{short} to \textbf{int}, \textbf{long}, \textbf{float}, or \textbf{double}
\item \textbf{char} to \textbf{int}, \textbf{long}, \textbf{float}, or \textbf{double}
\item \textbf{int} to \textbf{long}, \textbf{float}, or \textbf{double}
\item \textbf{long} to \textbf{float}, or \textbf{double}
\item \textbf{float} to \textbf{double}
\end{itemize}

Here is an example program to show casting of some of the data types:

\begin{code}{ex2b.java}
<< d['code/ex2b.java|pyg|l'] >>
\end{code}

Compile and run the code and you should receive the following output:

\begin{code}{ex2b.txt}
<< d['code/ex2b.txt|pyg|l'] >>
\end{code}

Some interesting things to point out, first is that the \textbf{float} to \textbf{double} 
conversion was not precise, this mostly has to do with how \textbf{float}s and 
\textbf{double}s are stored in binary and you should always keep this in mind.  The 
second is that the \textbf{long} to \textbf{double} conversion, when printed, is 
printed using scientific notation, which is not the case when we printed the \textbf{long} 
value in ex1a.

\section{Explicit Casting}

Another form of casting is Explicit Casting.  Using this method you are able to break out
of the limitations of downcasting, however, in many cases this causes a loss of precision 
in the number.  In the example below you can see that we are able to assign a \textbf{float}
value into an \textbf{int} variable.  Before running the code, what you guess the output
would be?

\begin{code}{ex2c.java}
<< d['code/ex2c.java|pyg|l'] >>
\end{code}

Compile and run the code and you should receive the following output:

\begin{code}{ex2c.txt}
<< d['code/ex2c.txt|pyg|l'] >>
\end{code}

Now that you've seen the output, did you successfully guess the output?  The reason for 
the output to just be 1 is that the cast of \textbf{float} to \textbf{int} causes just 
the integer part of the \textbf{float} value to be assigned.

\section{Extra Credit}

\begin{enumerate}
\item Open the ex2a file in your text editor and change the values being
	  assigned to each variable.
\item Create some additional variables and print them out.
\item Open the ex2b file in your text editor and try some of the other
 	  casting possibilities.
\item Try casting a type to some other type not listed in the rules above.
	  Make note of the error and try to fix the error by using a different
	  downcast.
\item Use the broken example from the previous extra credit and see if you 
	  can make it work with an explicit cast. 	 
\end{enumerate}




