\chapter{Exercise 0: The Setup}

In this chapter our primary goal will be to ensure that the Java  
Software Development Kit (SDK) is installed properly to allow
you to run all the example programs.  There are many different
flavors of Java SDK now that the platform has been open sourced.
Depending on the OS you run you may already have one installed
or have one distribution favorded over another.  Oracle Java is
the 'official' Java distribution and can be downloaded from
\href{http://www.oracle.com/technetwork/java/index.html}
{Oracle Technology Network for Java}.  Apple provides their own
Java distribution, for Lion you can download it at 
\href{http://support.apple.com/kb/DL1421}{Java for OS X Lion}.
If you run GNU/Linux chances are your distribution uses the
\href{http://openjdk.java.net}{OpenJDK} distribution.  These 
represent the three main Java distributions.

In all cases this first edition of the book will be focused on
the Java SE 6 (1.6) version of Java, even though Java SE 7 (1.7)
has been released.  So make sure you download the correct version.

\section{Windows}
Getting Java installed on Windows is as simple as downloading the
SDK installation file and double clicking.  Once complete you will
have the SDK as well as the Java Runtime Environment (JRE) installed.
At this point you can open up a command window and run the command
to get the version of Java installed.

\begin{code}{Check Java Installation on Windows}
\begin{lstlisting}
Microsoft Windows [Version 6.1.7600]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Users\user>java -version
java version "1.6.0_24"
Java(TM) SE Runtime Environment (build 1.6.0_24-b07)
Java HotSpot(TM) 64-Bit Server VM (build 19.1-b02, mixed mode)
\end{lstlisting}
\end{code}

Your version number may be slightly different, so long as you have 
1.6.0 as the first three numbers you will be good.

\section{Linux}
The easiest way to get Java installed in Linux is to use your 
distributions built in package manager.  Once you've installed
you can open up a terminal shell and run the command to get the
version of Java installed.

\begin{code}{Check Java Installation on Linux}
\begin{lstlisting}
$ java -version
java version "1.6.0_24"
Java(TM) SE Runtime Environment (build 1.6.0_24-b07)
Java HotSpot(TM) 64-Bit Server VM (build 19.1-b02, mixed mode)
\end{lstlisting}
\end{code}


\section{Apple OS X}
For Apple OS X you will need to download the Java release for
your version of OS X from \href{http://support.apple.com}
{Apple Support} and then install.  Once installed you can
open up a terminal shell and run the command to get the version
of Java installed.

\begin{code}{Check Java Installation on Apple OS X}
\begin{lstlisting}
$ java -version
java version "1.6.0_26"
Java(TM) SE Runtime Environment (build 1.6.0_26-b03-383-11A511c)
Java HotSpot(TM) 64-Bit Server VM (build 20.1-b02-383, mixed mode)
\end{lstlisting}
\end{code}



\section{Command Line / Terminal Shell}
If you are not comfortable using the Command Line or a Terminal Shell
(I will be refering both as the Shell from here on out) get ready, 
because you will use it quite frequently in this book.  Do
not be discouraged if you are slow at first, I can almost guarantee that
by the time you are finished you will be running through the shell like
a pro.

\section{Text Editor}

The choice of text editor for a programmer is a tough one.  For beginners
I tell them to just use \href{http://projects.gnome.org/gedit/}{Gedit} since
it's simple and works for code.  However, it doesn't work in certain
internationalized situations, and chances are you already have a favorite
text editor if you've been programming for a while.

With this in mind, I want you to try out a few of the standard programmer
text editors for your platform and then stick with the one that you like
best.  If you've been using GEdit and like it then stick with it.  If you
want to try something different, then try it out real quick and pick one.

The most important thing is \emph{do not get stuck picking the perfect editor}.
Text editors all just kind of suck in odd ways.  Just pick one, stick with it,
and if you find something else you like try it out.  Don't spend days
on end configuring it and making it perfect.

Some text editors to try out are:

\begin{enumerate}
\item \href{http://projects.gnome.org/gedit/}{Gedit} on Linux and OSX.
\item \href{http://www.barebones.com/products/textwrangler/}{TextWrangler} on OSX.
\item \href{http://www.nano-editor.org/}{Nano} which runs in Terminal and works nearly everywhere.
\item \href{http://www.gnu.org/software/emacs/}{Emacs} and \href{http://emacsformacosx.com/}{Emacs for OSX}.  Be prepared to do some learning though.
\item \href{http://www.vim.org/}{Vim} and \href{http://code.google.com/p/macvim/}{MacVim}
\end{enumerate}

There is probably a different editor for every person out there, but these are
just a few of the free ones that I know work.  Try a few out, and maybe some
commercial ones until you find one that you like.

\section*{Integrated Development Environments (IDEs)}

IDEs can be useful tools, they have loads of automation features, nice debugging
facilities and other cool tools like Intellisence to help you along.  For the 
purposes of this book \emph{DO NOT USE ONE!}.  There will be no examples in this book
that even comes close to requiring an IDE in order to get running properly.  In 
fact I would even go as far to say that trying to use an IDE for any of the examples
will actually slow you down and hamper the learning process.  I want you to be typing
every example, word for word, that you find in this book.  Repetition is part of the
learning process.  Having Intellisence poping up offering to fill syntax in for you
will hinder this greatly.  To reiterate do not worry about IDEs right now, you will
not need one.
